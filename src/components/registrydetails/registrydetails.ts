import { Component, ViewChild } from '@angular/core';
import { BaseRestService } from 'src/providers/base.rest.service';
import { RenderDebugInfo } from '@angular/core/src/render/api';
import { Router, ActivatedRoute } from '@angular/router';
import { PojoService } from 'src/providers/services/pojo.service';
import { StorageService } from 'src/providers/storageservice/storageservice';



@Component({
    selector: 'registrydetails',
    templateUrl: 'registrydetails.html',

})

export class RegistryDetails {

    public imageDetails: any;
    public sltdimageDetails: any;
    constructor(private pojoservice: PojoService, private storageService: StorageService, private router: Router) {
        let uuid = this.storageService.get('registry_uuid');
        if (!uuid) {
            this.router.navigateByUrl('Login');
        }
    }

    ngOnInit(): void {
        this.imageDetails = this.pojoservice.getimage();
        this.sltdimageDetails = this.pojoservice.getslctdimage();

        console.log(this.imageDetails);
        console.log(this.sltdimageDetails)
    }
}