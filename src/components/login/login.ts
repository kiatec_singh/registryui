import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BaseRestService } from 'src/providers/base.rest.service';
import { StorageService } from 'src/providers/storageservice/storageservice';

@Component({
    selector: 'login',
    templateUrl: 'login.html',

})

export class Login {

    private userkey;
    private passwordkey;
    private error=false;

    constructor(private router:Router,private baserestService:BaseRestService, private storageService:StorageService) {

    }

    ngOnInit(): void {
       
    }
    login(){
        this.baserestService.login(this.userkey,this.passwordkey).then(
            data=>{ 
                let udata:any=data;
                this.storageService.set('registry_uuid',udata.id);
                this.router.navigateByUrl('Registry');
            },
            error=>{this.error=true}
        )
    }
}