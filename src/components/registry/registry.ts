import { Component, ViewChild } from '@angular/core';
import { BaseRestService } from 'src/providers/base.rest.service';
import { RenderDebugInfo } from '@angular/core/src/render/api';
import { Router } from '@angular/router';
import { PojoService } from 'src/providers/services/pojo.service';
import { StorageService } from 'src/providers/storageservice/storageservice';

@Component({
    selector: 'registry',
    templateUrl: 'registry.html',

})

export class Registry {

    public registries;
    public allregistrylist=[];
    public imagestodelete=[];
    public slctItem;
    constructor(private baserestService: BaseRestService,private storageService:StorageService,private router:Router,private pojoService:PojoService) {

let uuid =this.storageService.get('registry_uuid');
if(!uuid){
    this.router.navigateByUrl('Login');
}
    }

    ngOnInit(baes): void {
        this.baserestService.getCatalog().subscribe(
            registry => { this.registries = registry, this.setData() },
            error => { console.log(error) }

        );
    }
    setData() {
            let allregistries=[];
        for (let i in this.registries.repositories) {
            this.baserestService.getCatalogDetals(this.registries.repositories[i]).subscribe(
                allregistry => { allregistries.push(allregistry)},
                error => { console.log(error) }

            );
        }
         this.renderUI(allregistries) ;

    }
    checkedItem(e,item:any){
        let slctitem:any = item.name
        if(e.target.checked===true){
            this.imagestodelete.push(slctitem)
        }
        else{
            this.imagestodelete.splice(this.imagestodelete.indexOf(slctitem), 1 );
        }
       
    }
    renderUI(allregistries) {
        this.allregistrylist =allregistries;
    }
    getManifests(e,item){
        this.slctItem= item;
        this.baserestService.getCatalogManifests(item.name,item.tags[0]).subscribe(
            allregistrymanifets => { console.log(allregistrymanifets), this.imageDetailsPage(allregistrymanifets)},
            error => { console.log(error) }


        ); 
    }
    imageDetailsPage(allregistrymanifets){
        this.pojoService.Setimage(allregistrymanifets);
        this.pojoService.Setslctdimage(this.slctItem);
        this.router.navigateByUrl('RegistryDetails',{ state: {selctItem: this.slctItem }});
    }

}