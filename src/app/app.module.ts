//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationStrategy, HashLocationStrategy, APP_BASE_HREF, DatePipe } from '@angular/common';


//pages
import { HeaderPage } from '../components/header/header-page';
import { FooterPage } from '../components/footer/footer-page';
import { MenuRight } from '../components/menu-right/menu-right';
import { MenuLeft } from '../components/menu-left/menu-left';
import { Navigation } from '../components/navigation/navigaton';
import { Registry } from '../components/registry/registry';
import { RegistryDetails } from '../components/registrydetails/registrydetails';
import { Login } from '../components/login/login';

//services
import { BaseRestService } from '../providers/base.rest.service';
import { PojoService } from '../providers/services/pojo.service';
import { StorageService } from '../providers/storageservice/storageservice';



//components
import { AppComponent } from './app.component';




//directives
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';




const modelDeclatarions = [

]

const pagesDeclarations = [

]

const componentsDeclarations = [
  HeaderPage,
  FooterPage,
  MenuLeft,
  MenuRight,
  Navigation,
  Registry,
  RegistryDetails,
  Login,
]

const providersDeclarations = [
  BaseRestService,
  PojoService,
  StorageService,
]
const pipeDeclarations = []

const directivesDeclarations = [
  
]


@NgModule({
  declarations: [
    AppComponent, 
    // OwlDateTimeModule, 
    // OwlMomentDateTimeModule,
    //globalization,
    //globalize,
    ...pagesDeclarations,
    // Models
    ...modelDeclatarions,
    // Components
    ...componentsDeclarations,
    // Directives
    ...directivesDeclarations,
    // Pipes
    ...pipeDeclarations

  ],
  entryComponents: [
 

  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot([]),

  ],
  providers: [
    ...providersDeclarations,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: APP_BASE_HREF, useValue: '/' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
