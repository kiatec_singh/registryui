import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistryDetails } from '../components/registrydetails/registrydetails';
import { Registry } from 'src/components/registry/registry';
import { Login } from 'src/components/login/login';

const routes: Routes = [
    { path: 'RegistryDetails', component: RegistryDetails },
    { path: 'Registry', component: Registry },
    { path: '', component: Login },
    { path: 'Login', component: Login },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {
    ngOnInit() {
        console.log("in routereee....");
    }
};