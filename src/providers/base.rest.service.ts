import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Form } from '@angular/forms';
import { Subscriber } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class BaseRestService {
    headers: Headers;
    private options;
    form: Form;
    private authorisation;
    private subscriber: Subscriber<any>
    private registryservicesUrl = 'https://udv-researchdevelopment.carex.dk/webservice/api.php';


    // URL to web api
    constructor(private http: HttpClient) {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
            'Accept': 'application/json',
            "Access-Control-Allow-Headers": "X-Requested-With"

        });

        this.options = new RequestOptions({ headers: this.headers });

    }
    private formdata;

    getCatalog(): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'getcatalog');
        return this.http.post(this.registryservicesUrl, this.formdata).pipe(map(res => res));
    }
    getCatalogDetals(image): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'getcatalogdetails');
        this.formdata.append('image', image);
        return this.http.post(this.registryservicesUrl, this.formdata).pipe(map(res => res));
    }
    login(username, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'login');
        this.formdata.append('id', username);
        this.formdata.append('password', password);
        return this.http.post('https://uat-idp.carex.dk/endpoints/login_services.php', this.formdata, this.options).toPromise();
    }
    getCatalogManifests(image, tag): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'getcatalogmanifests');
        this.formdata.append('tag', tag);
        this.formdata.append('image', image);
        return this.http.post(this.registryservicesUrl, this.formdata).pipe(map(res => res));
    }
}

// https://registry.carex.dk:5000/v2/autorisation/tags/list

// curl --insecure https://registry.carex.dk:5000/v2/autorisation/manifests/latest