import { Injectable } from '@angular/core';


@Injectable()
export class PojoService {
    public image;
    public slctdimage;

    constructor() {


    }
    Setimage(image) {
        this.image = image;

    }
    getimage() {
        return this.image
    }
    Setslctdimage(slctdimage) {
        this.slctdimage = slctdimage;
    }
    getslctdimage() {
        return this.slctdimage
    }
}